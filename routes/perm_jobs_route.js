var _ = require('lodash'); 
var permjob = require('../models/perm_jobs_model.js');


module.exports = function(app){
    
    _permjobs = [];
    
    //create
    app.post('/permjob', function(req, res){
        var newpermjob = new permjob(req.body);
        newpermjob.save(function(err){
            if(err){
                res.send({info: 'error creating ', error:err});
            }
            res.json({info: 'permjob created succesfully'});
        })
     
    });
    
    //read
    app.get('/permjob', function(req, res){
        permjob.find(function(err, permjobs){
            if(err){
                res.json({info: 'error getting permjobs', error: err});
            }
            res.json({info: 'permjobs found', data:permjobs});
        })
    })
    
    
    app.get('/permjob/:id', function(req, res){
      permjob.findById(req.params.id, function(err, cat){
          if(err){
            res.json({info: 'error getting permjob', error: err});
          }
          if(permjob){
            res.json({info: 'permjob found', data: permjob})

          }
          else{
            res.json({info: 'cat not found'});
          }
      });
    });
    
    //update
    app.put('/permjob/:id', function(req, res){
       permjob.findById(req.params.id, function(err, permjob){
          if(err){
              res.json({info: 'error finding permjob', error: err})
          }
           if(permjob){
               _.merge(permjob, req.body);
               permjob.save(function(err){
                   if(err){
                       res.json({info: 'error during permjob update', error: err});
                   }
                   res.send({info: 'permjob updated succesfully'});
               })
           }else{
               res.info({info: 'permjob not found'});
           }
            
       });
        
        
    });
    
    // delete
    app.delete('/permjob/id', function(req, res){
        permjob.findByIdAndRemove(req.params.id, function(err){
            if(err){
                res.json({info: 'error deleting permjob', error: err});
            };
            res.json({info: 'permjob deleted'});
        })
    })
    
    
    
    
}