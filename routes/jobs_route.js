var _ = require('lodash'); 
var Job = require('../models/jobs_model');


module.exports = function(app){
    
    _jobs = [];
    
    //create
    app.post('/job', function(req, res){
        var newJob = new Job(req.body);
        newJob.save(function(err){
            if(err){
                res.send({info: 'error creating ', error:err});
            }
            res.json({info: 'job created succesfully'});
        })
     
    });
    
    //read
    app.get('/job', function(req, res){
        Job.find(function(err, jobs){
            if(err){
                res.json({info: 'error getting jobs', error: err});
            }
            res.json({info: 'jobs found', data:jobs});
        })
    })
    
    
    app.get('/job/:id', function(req, res){
      Job.findById(req.params.id, function(err, cat){
          if(err){
            res.json({info: 'error getting job', error: err});
          }
          if(job){
            res.json({info: 'job found', data: job})

          }
          else{
            res.json({info: 'cat not found'});
          }
      });
    });
    
    //update
    app.put('/job/:id', function(req, res){
       Job.findById(req.params.id, function(err, job){
          if(err){
              res.json({info: 'error finding job', error: err})
          }
           if(job){
               _.merge(job, req.body);
               job.save(function(err){
                   if(err){
                       res.json({info: 'error during job update', error: err});
                   }
                   res.send({info: 'job updated succesfully'});
               })
           }else{
               res.info({info: 'job not found'});
           }
            
       });
        
        
    });
    
    // delete
    app.delete('/job/id', function(req, res){
        Job.findByIdAndRemove(req.params.id, function(err){
            if(err){
                res.json({info: 'error deleting job', error: err});
            };
            res.json({info: 'job deleted'});
        })
    })
    
    
    
    
}