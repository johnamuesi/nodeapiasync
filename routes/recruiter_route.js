var r = require('request').defaults({
    json:true
});

// async library
var async = require('async');

module.exports = function(app){
  
    //read
    app.get('/jobs', function(req, res){
       
        async.parallel({
           
            job: function(callback){
                r({uri: 'http://localhost:3000/job', function(error, response, body){
                    if(error){
                        callback({service: 'job', error: error});// error
                    };
                    if(!error && response.statusCode === 200){// success
                        callback(null, body); // callback with error nulled and data in body
                    }
                    else{
                        callback(response.statusCode); // callback with whatever status code
                    }
                }})
            },
            
            permjob: function(callback){
                  r({uri: 'http://localhost:3001/permjob', function(error, response, body){
                    if(error){
                        callback({service: 'job', error: error});// error
                    };
                    if(!error && response.statusCode === 200){// success
                        callback(null, body); // callback with error nulled and data in body
                    }
                    else{
                        callback(response.statusCode); // callback with whatever status code
                    }
                }})
                
            }
                   
        });
             
    });
    
};