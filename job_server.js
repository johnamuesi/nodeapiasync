//// pure node 
//var http = require('http');
//
//
//http.createServer(function(req, res){
//    res.writeHead(200, {
//        'Content-Type': 'text/plain'
//    });
//    res.end('Hi John\n');
//}).listen(3000, '127.0.0.1');
//
//console.log('Server running at http://127.0.0.1:3000');
//

//express
var express = require('express');
var app = express();

// for adding data to request
var bodyParser = require('body-parser');


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/jobs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));


var jobs = require('./routes/jobs_route.js')(app);


app.get('/', function(req, res){
    res.json('hello bio agency');
    
});

var server = app.listen(3000, function(){
    console.log('server running at http://127.0.0.1');
})