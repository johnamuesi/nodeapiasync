
//express
var express = require('express');
var app = express();

// for adding data to request
var bodyParser = require('body-parser');


var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/permanent');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:true
}));

var jobs = require('./routes/perm_jobs_route.js')(app);


app.get('/', function(req, res){
    res.json('hello bio agency');
    
});

var server = app.listen(3001, function(){
    console.log('server running at http://127.0.0.1:3001');
})