var mongoose = require('mongoose');

var jobSchema = mongoose.Schema({
    name:String,
    rate:Number,
    location:String
});

module.exports = mongoose.model('Job', jobSchema);