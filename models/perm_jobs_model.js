var mongoose = require('mongoose');

var permjobSchema = mongoose.Schema({
    name:String,
    rate:Number,
    location:String
});

module.exports = mongoose.model('PermJob', permjobSchema);